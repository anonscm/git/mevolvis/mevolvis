package de.tarent.mevolvis;

import java.util.Hashtable;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPUpload {

	JSch jsch = new JSch();
	private Session sftpSession;
	private String host = null;
	private String user;
	private String password;
	int port = 22;
	private String destination = null;
	private String filePath = null;

	/**
	 * Handles file upload to evolvis through sftp
	 * 
	 * @param evolvisId
	 * @param evolvisInst
	 * @param filePath
	 */
	public SFTPUpload(String evolvisId, String evolvisInst, String filePath, String username, String password) {
		jsch = new JSch();
		this.host = evolvisInst;
		this.filePath = filePath;
		this.user = username;
		this.password = password;
		
		this.destination = "/var/lib/gforge/chroot/home/groups/" + evolvisId + "/incoming/";
	}

	/**
	 * Upload files to evolvis
	 */
	public void performUpload() {
		try {
			// create Session
			sftpSession = jsch.getSession(user, host, port);

			Hashtable config = new Hashtable();
			config.put("StrictHostKeyChecking", "no");
			sftpSession.setConfig(config);
			sftpSession.setPassword(password);

			sftpSession.connect();

			ChannelSftp sftpChannel = (ChannelSftp) sftpSession.openChannel("sftp");
			sftpChannel.connect();

			if (sftpChannel.isConnected()) {
				try {
					//upload source file to destination
					sftpChannel.put(filePath, destination);

				} catch (SftpException e) {
					// TODO handle Exception
					e.printStackTrace();
				}
			} else {
				//TODO exception
			}

			sftpSession.disconnect();
		} catch (JSchException e) {
			// TODO handle Exception
			e.printStackTrace();
		}
	}
}