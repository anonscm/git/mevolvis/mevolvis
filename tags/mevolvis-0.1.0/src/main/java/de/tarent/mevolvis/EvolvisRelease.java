package de.tarent.mevolvis;

import java.io.File;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.maven.plugin.MojoFailureException;
import org.evolvis.wsclient.gen.FRSFile;
import org.evolvis.wsclient.gen.FRSPackage;
import org.evolvis.wsclient.gen.FRSRelease;
import org.evolvis.wsclient.gen.GForgeAPIBindingStub;
import org.evolvis.wsclient.gen.GForgeAPIPortType;
import org.evolvis.wsclient.gen.Group;
import org.evolvis.wsclient.gen.GroupSCMData;
import org.evolvis.wsclient.gen.ProjectTask;

public class EvolvisRelease {

	private Logger log = Logger.getLogger("EvolvisRelease");

	private EvolvisConnection connection;
	private GForgeAPIPortType bindingStub;
	private String evolvisId;
	private String instance;
	private String releaseNotes;
	private String filePath;
	private String releasePackageName;
	private int fileTypeId;
	private String changes;
	private int releaseId;
	private int packageId = 0;
	private Group[] groups;
	private String releaseName;

	public String getReleaseName() {
		return releaseName;
	}

	public void setReleaseName(String releaseName) {
		this.releaseName = releaseName;
	}

	private File releaseFile;

	public EvolvisRelease(String instance, String evolvisId,
			String releaseNotes, String filePath, EvolvisConnection connection,
			String releasePackage, int fileTypeId, String changes) {
		this.connection = connection;
		this.bindingStub = connection.getBindingStub();
		this.evolvisId = evolvisId;
		this.instance = instance;
		this.releaseNotes = releaseNotes;
		this.filePath = filePath;
		this.releasePackageName = releasePackage;
		this.fileTypeId = fileTypeId;
		this.changes = changes;
	}

	public boolean PrepareRelease() throws RemoteException {
		String[] groupnames = { evolvisId };

		try {
			groups = bindingStub.getGroupsByName(connection.getSession(),
					groupnames);
		} catch (RemoteException e) {
			// TODO: handle exception
			return false;
		}

		releaseFile = new File(filePath);
		releaseName = releaseFile.getName();

		FRSPackage[] packages = bindingStub.getPackages(
				connection.getSession(), groups[0].getGroup_id());

		boolean packageExists = false;
		for (int i = 0; i < packages.length; i++) {

			if (releasePackageName.equals(packages[i].getName())) {
				// package exists
				log.log(Level.INFO, "Adding file to existing package");
				packageExists = true;
				packageId = packages[i].getPackage_id();

			} else if (releasePackageName.isEmpty() && !packageExists) {
				// packagename-parameter was empty so just use the projects name
				// as packagename
				log.log(Level.INFO,
						"No package name specified. Adding release to default package.");

				packageId = bindingStub.addPackage(connection.getSession(),
						groups[0].getGroup_id(), groups[0].getGroup_name(), 1);
			} else if (i == packages.length - 1 && !packageExists) {
				// package doesn't exists so create it
				log.log(Level.INFO,
						"Package doesn't exist. Create new package "
								+ releasePackageName);
				packageId = bindingStub.addPackage(connection.getSession(),
						groups[0].getGroup_id(), releasePackageName, 1);

			}
		}

		// check if the already is a release with the same name
		FRSRelease[] releaseArray = bindingStub.getReleases(
				connection.getSession(), groups[0].getGroup_id(), packageId);
		for (int i = 0; i < releaseArray.length; i++) {
			if (releaseArray[i].getName().equals(releaseName)) {
				releaseName = releaseName + Math.random();
				// return false;
			}
		}

		return true;
	}

	public void performRelease() throws RemoteException {
		// upload file
		SFTPUpload upload = new SFTPUpload(evolvisId, instance, filePath,
				connection.getUsername(), connection.getPassword());
		upload.performUpload();

		int time = (int) (System.currentTimeMillis() / 1000L);
		if (packageId != 0) {
			// add release to evolvis
			releaseId = bindingStub.addRelease(connection.getSession(),
					groups[0].getGroup_id(), packageId, releaseName,
					releaseNotes, changes, time);

			// add the uploaded file to the new created release
			bindingStub.addUploadedFile(connection.getSession(),
					groups[0].getGroup_id(), packageId, releaseId,
					releaseFile.getName(), fileTypeId, 9999, time);
		}
	}
}
