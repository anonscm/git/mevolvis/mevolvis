package de.tarent.mevolvis;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Uploads a release to evolvis
 * 
 * @goal upload
 * 
 * @phase package
 */
public class MyMojo extends AbstractMojo {

	/**
	 * Specifies the evolvis-instance
	 * 
	 * @parameter expression="${upload.instance}" default-value="evolvis.org"
	 */
	private String instance;

	/**
	 * Specifies the evolvis-ID
	 * 
	 * @parameter expression="${upload.evolvisId}" default-value="000000"
	 */
	private String evolvisId;

	/**
	 * Path to releasenotes
	 * 
	 * @parameter expression="${upload.releaseNotesPath}"
	 *            default-value="target/notes.txt"
	 */
	private String releaseNotesPath;

	/**
	 * Path to change file
	 * 
	 * @parameter expression="${upload.changesPath}"
	 *            default-value="target/changes.txt"
	 */
	private String changesPath;

	/**
	 * Package of the release
	 * 
	 * @parameter expression="${upload.releasePackage}"
	 *            default-value="release-Package"
	 */
	private String releasePackage;

	/** @parameter default-value="${project}" */
	private org.apache.maven.project.MavenProject mavenProject;

	private EvolvisConnection connection;

	public EvolvisConnection getConnection() {
		return connection;
	}

	public void setConnection(EvolvisConnection connection) {
		this.connection = connection;
	}

	public void execute() throws MojoExecutionException, MojoFailureException {
		String releaseNotes = null;

		// check if path for releasenotes is valid
		if (new File(releaseNotesPath).exists()) {
			releaseNotes = readNotes(releaseNotesPath);
		} else {
			getLog().info("No releasnotes attached");
			releaseNotes = " ";
		}

		String filePath = mavenProject.getBuild().getDirectory() + "/"
				+ mavenProject.getBuild().getFinalName() + "."
				+ mavenProject.getPackaging();

		String changes = " ";
		// check if path for changes file is valid
		// if (new File(changesPath).exists()) {
		// changes = readNotes(changesPath);
		// getLog().info(changes);
		// } else {
		// getLog().info("No changes file attached");
		// changes = " ";
		// getLog().info("1111111 Changes: " + changes);
		// }

		String packaging = mavenProject.getPackaging();
		// evolvis identifies the packaging only by id, not by name. So it's
		// nessecary to map the string to the associated id
		int fileTypeId = mapFiletype(packaging);

		// create Connection to evolvis
		connection = new EvolvisConnection();
		try {
			connection.connectToEvolvis(instance);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MojoFailureException(
					"There's a problem with the properties-file. Please make sure that it exists.");

		}

		EvolvisRelease release = null;
		if (new File(filePath).exists()) {
			// create release from parameters
			release = new EvolvisRelease(instance, evolvisId, releaseNotes,
					filePath, connection, releasePackage, fileTypeId, changes);
		} else {
			throw new MojoFailureException("File "
					+ mavenProject.getBuild().getFinalName()
					+ " couldn't be found.");
		}

		try {
			if (release.PrepareRelease()) {
				release.performRelease();
			} else {
				throw new MojoFailureException(
						"Release for  "
								+ release.getReleaseName()
								+ " failed. Please check the config of the mevolvis-plugin in your pom.xml.");
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Maps the filetype parameter to the required fileTypeId
	 * 
	 * @return fileTypeId four digit id which identifies the filetype
	 */
	private int mapFiletype(String packaging) {
		if (packaging.equals(".deb") || packaging.equals("deb")) {
			return 1000;
		} else if (packaging.equals(".zip") || packaging.equals("zip")) {
			return 3000;
		} else if (packaging.equals(".gz") || packaging.equals("gz")
				|| packaging.equals(".tar.gz") || packaging.equals("tar.gz")) {
			return 3110;
		} else if (packaging.equals("source") || packaging.equals("sourcefile")) {
			return 5900;
		} else {
			return 9999;
		}
	}

	/**
	 * Read the content of the releasenotes file
	 * 
	 * @param releaseNotesPath
	 * @return Releasenotes as String
	 */
	private String readNotes(String releaseNotesPath) {
		FileInputStream fis;
		String releaseNotes;
		// create new FIS-Object and check if the given path is valid
		try {
			fis = new FileInputStream(releaseNotesPath);
		} catch (FileNotFoundException e) {
			return null;
		}

		String line;
		StringBuffer text = new StringBuffer("");
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(fis));
			// read every line of the file
			while ((line = br.readLine()) != null) {
				// text.append(line + "\n");
				text.append(line);
			}
			fis.close();
			br.close();
		} catch (Exception e) {
			return null;
		}
		// convert Stringbuffer to String
		releaseNotes = text.toString();
		return releaseNotes;
	}
}
