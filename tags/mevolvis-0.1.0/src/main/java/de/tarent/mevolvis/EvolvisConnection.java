package de.tarent.mevolvis;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Properties;

import javax.xml.rpc.ServiceException;

import org.evolvis.wsclient.gen.GForgeAPILocator;
import org.evolvis.wsclient.gen.GForgeAPIPortType;

public class EvolvisConnection {

	public String session;
	private GForgeAPIPortType bindingStub = null;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String password;

	public GForgeAPIPortType getBindingStub() {
		return bindingStub;
	}

	public void setBindingStub(GForgeAPIPortType bindingStub) {
		this.bindingStub = bindingStub;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	/**
	 * creates connection to evolvis and logs in
	 * 
	 * @param evolvisInstance
	 * @throws IOException
	 */
	public void connectToEvolvis(String evolvisInstance) throws IOException {
		GForgeAPILocator apiLocator = new GForgeAPILocator();

		// At the moment, evolvis doesn't support SSO. Username and password are
		// stored in a properties file until SSO is supported
		Properties properties = new Properties();
		BufferedInputStream stream = new BufferedInputStream(
				new FileInputStream(
						"/home/joscha/workspace/mevolvis.properties"));
		properties.load(stream);
		stream.close();
		username = properties.getProperty("username");
		password = properties.getProperty("password");
		

		URL url = null;
		try {
			url = new URL("http://" + evolvisInstance + "/soap/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bindingStub = apiLocator.getGForgeAPIPort(url);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			session = bindingStub.login(username, password);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
