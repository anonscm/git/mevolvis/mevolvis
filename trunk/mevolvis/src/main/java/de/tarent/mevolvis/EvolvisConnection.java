package de.tarent.mevolvis;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.xml.rpc.ServiceException;

import org.evolvis.wsclient.gen.FusionForgeAPILocator;
import org.evolvis.wsclient.gen.FusionForgeAPIPortType;

public class EvolvisConnection {
	
	private Logger log = Logger.getLogger("EvolvisConnection");

	public String session;

	private FusionForgeAPIPortType bindingStub = null;
	
	private String username;

	private String password;
	
	/** path to an alternative configuration file with evolvis credentials */
	private static final String alternativePropertiesPath ="/opt/evolvis.properties";
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public FusionForgeAPIPortType getBindingStub() {
		return bindingStub;
	}

	public void setBindingStub(FusionForgeAPIPortType bindingStub) {
		this.bindingStub = bindingStub;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	private String getAlternativePropertiesPath() {
		return alternativePropertiesPath;
	}
	
	private Properties getConfigFileProperties(String propertiesPath) {
		Properties properties = new Properties();
		
		BufferedInputStream stream = null;
		
		try {
			
			stream = new BufferedInputStream(new FileInputStream(propertiesPath));
			properties.load(stream);
			stream.close();
			
			log.info("The data file including the evolvis credentials was found.");
			
		} catch(FileNotFoundException e) {
			log.info("The data file including the evolvis credentials was not found under the following path: "+propertiesPath);
		} catch (Exception e) {
			log.info("An unknown error occured.");
		}
			
		return properties;
	}

	/**
	 * creates connection to evolvis and logs in
	 * 
	 * @param evolvisInstance
	 * @throws IOException
	 */
	public void connectToEvolvis(String evolvisInstance, String propertiesPath, boolean useSSL) throws IOException {
		FusionForgeAPILocator apiLocator = new FusionForgeAPILocator();

		// At the moment, evolvis doesn't support SSO. Username and password are
		// stored in a properties file until SSO is supported
		Properties properties = new Properties();
		
		properties = getConfigFileProperties(propertiesPath);
		if(properties.isEmpty()) {
			properties = getConfigFileProperties(getAlternativePropertiesPath());
		}
			
		if(!properties.isEmpty()) {
			
			username = properties.getProperty("username");
			password = properties.getProperty("password");
		
			if(username == null || username.toString().trim().equals("")) {
				log.info("No username available in the properties data file.");
			}
		
			if(password == null || password.toString().trim().equals("")) {			
				log.info("No password available in the properties data file.");
			}
		}
		
		URL url = null;
		String protocol = "http";
		if(useSSL) {
			protocol += "s";
		}
		
		try {
			url = new URL(protocol+"://" + evolvisInstance + "/soap/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bindingStub = apiLocator.getFusionForgeAPIPort(url);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			session = bindingStub.login(username, password);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
